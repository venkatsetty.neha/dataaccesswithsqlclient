﻿using System;
using SQLClientCRUD.Repositories;
using SQLClientCRUD.Models;
using System.Collections;
using System.Collections.Generic;

namespace SQLClientCRUD
{
  public  class Program
    {
        static void Main(string[] args)
        {
           
            // Simulating Dependency Injection (highly simplified)
            ICustomerRepository repository = new CustomerRepository();
            // Passing dependency
            TestSelectAll(repository);
            //TestSelect(repository);
            TestAdd(repository);
            TestUpdate(repository);
        }
        static void TestSelectAll(ICustomerRepository repository)
        {
            PrintCustomers(repository.GetAllCustomers());
        }


        static void TestInsert(ICustomerRepository repository)
        {

        }
        static void TestAdd(ICustomerRepository repository)
        {
            Customer obj = new Customer();
            obj.FirstName = "sai";

            repository.AddNewCustomer(obj);

        }

        static void TestUpdate(ICustomerRepository repository)
        {
            Customer obj = new Customer();
            obj.FirstName = "sai";

            repository.AddNewCustomer(obj);
        }

        static void TestDelete(ICustomerRepository repository)
        {

        }

        static void PrintCustomers(IEnumerable<Customer> customers)
        {
            foreach (Customer customer in customers)
            {
                PrintCustomer(customer);
            }
        }

        static void PrintCustomer(Customer customer)
        {
            Console.WriteLine($"--- { customer.CustomerID} {customer.FirstName} {customer} {customer} ---");
        }

    }
}
