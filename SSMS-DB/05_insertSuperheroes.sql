USE [SuperheroesDb]
GO


/*******************************************************************************
   Populate Superhero Table
********************************************************************************/




INSERT INTO [dbo].[Superhero] ([Name], [Alias], [Origin]) VALUES (N'Spider Man', N'Peter Parker', N'Queens' );
INSERT INTO [dbo].[Superhero] ([Name], [Alias], [Origin]) VALUES (N'Wonder Woman', N'Diana Princer', N'Themyscira' );
INSERT INTO [dbo].[Superhero] ([Name], [Alias], [Origin]) VALUES (N'Bat Man', N'Caped Crusaderr', N'Gotham City' );


GO